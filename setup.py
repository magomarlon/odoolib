#!/usr/bin/env python

from distutils.core import setup

setup(name='odoolib',
      version='0.5',
      description='Odoo XML-RPC Easy access library',
      author='Juan Hernandez',
      author_email='juan@qn.co.ve',
      url='http://qn.co.ve/odoolib/', 
      packages=['odoolib'])

